﻿using System;
using System.Collections.Generic;

using Yamabuki.Design.Component.Base;
using Yamabuki.Window.ChildForm;

namespace Yamabuki.Form.Job
{
    public interface JobForm_VI
        : ChildForm_VI
    {
        event Action<Boolean> Form_VisibleChanged;

        DsComponentGuid SelectedNodeGuid { get; }

        Action Form_Load { set; }

        Action JobTreeView_DoubleClick { set; }

        JobNameEditEventHandler JobTreeView_AfterJobNameEdit { set; }

        ComponentNameEditEventHandler JobTreeView_AfterComponentNameEdit { set; }

        ComponentSelectEventHandler JobTreeView_AfterComponentSelect { set; }

        Action<String> ComponentListUpdateEventaHandler { set; }

        Action<String> ComponentListBox_DoubleClick { set; }

        void Clear();

        void AddJobNode(DsComponentGuid guid, String name);

        void RemoveJobNode(DsComponentGuid guid);

        void AddComponentNode(DsComponentGuid parentGuid, DsComponentGuid targetGuid, String name);
        
        void RemoveComponentNode(DsComponentGuid guid);

        void UpdateNodeName(String oldPath, String newPath);

        void SetComponentListBox(IEnumerable<String> valueList);

        void SelectNode(DsComponentGuid guid);
    }
}
