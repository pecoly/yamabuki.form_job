﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Windows.Forms;

using WeifenLuo.WinFormsUI.Docking;

using Yamabuki.Design.Component.Base;
using Yamabuki.Design.Constant;
using Yamabuki.Window.ChildForm;
using Yamabuki.Window.Utility;

namespace Yamabuki.Form.Job
{
    public delegate void ComponentNameEditEventHandler(
        out Boolean cancel,
        String oldPath,
        String newPath,
        String newName);

    public delegate void JobNameEditEventHandler(
        out Boolean cancel,
        String oldPath,
        String newPath,
        String newName);

    public delegate void ComponentSelectEventHandler(String path);

    public partial class JobForm_V
        : ChildForm_V, JobForm_VI
    {
        private const String Job = "ジョブ";
                
        private EventHandler form_Load;
        
        private EventHandler jobTreeView_DoubleClick;
        
        private JobNameEditEventHandler afterJobNameEditEventHandler;

        private ComponentNameEditEventHandler afterComponentNameEditEventHandler;

        private ComponentSelectEventHandler afterComponentSelectEventHandler;

        private EventHandler componentListBox_DoubleClick;

        private Boolean doubleClicked;

        private TreeNode projectNode;

        private DsComponentGuid programGuid = new DsComponentGuid();

        private Dictionary<DsComponentGuid, TreeNode> nodeList = new Dictionary<DsComponentGuid, TreeNode>();

        public JobForm_V()
        {
            this.InitializeComponent();
            this.State = this.DefaultState;
        }

        public DsComponentGuid SelectedNodeGuid
        {
            get
            {
                var node = this.jobTreeView.SelectedNode;
                if (node == null)
                {
                    return null;
                }

                foreach (var kv in this.nodeList)
                {
                    if (kv.Value == node)
                    {
                        return kv.Key;
                    }
                }

                return null;
            }
        }

        public override DockState DefaultState
        {
            get { return DockState.DockLeft; }
        }

        public Action Form_Load
        {
            set
            {
                this.Load -= this.form_Load;
                this.form_Load = (sender, e) => value();
                this.Load += this.form_Load;
            }
        }

        public Action JobTreeView_DoubleClick
        {
            set
            {
                this.jobTreeView.DoubleClick -= this.jobTreeView_DoubleClick;
                this.jobTreeView_DoubleClick = (sender, e) => value();
                this.jobTreeView.DoubleClick += this.jobTreeView_DoubleClick;
            }
        }

        public JobNameEditEventHandler JobTreeView_AfterJobNameEdit
        {
            set { this.afterJobNameEditEventHandler = value; }
        }

        public ComponentNameEditEventHandler JobTreeView_AfterComponentNameEdit
        {
            set { this.afterComponentNameEditEventHandler = value; }
        }

        public ComponentSelectEventHandler JobTreeView_AfterComponentSelect
        {
            set { this.afterComponentSelectEventHandler = value; }
        }

        public Action<String> ComponentListUpdateEventaHandler { private get; set; }

        public Action<String> ComponentListBox_DoubleClick
        {
            set
            {
                this.componentListBox.ListBox.DoubleClick -= this.componentListBox_DoubleClick;
                this.componentListBox_DoubleClick = (sender, e) =>
                    {
                        var name = this.componentListBox.SelectedItem as String;
                        Console.WriteLine(name);
                        value(name);
                    };
                this.componentListBox.ListBox.DoubleClick += this.componentListBox_DoubleClick;
            }
        }

        public override String PersistString
        {
            get { return "Yamabuki.Form.Job.JobForm"; }
        }

        public override void Initialize()
        {
            this.jobTreeView.BeforeExpand += this.JobTreeView_BeforeExpand;
            this.jobTreeView.BeforeCollapse += this.JobTreeView_BeforeCollapse;
            this.jobTreeView.MouseDown += this.JobTreeView_MouseDown;
            this.jobTreeView.BeforeLabelEdit += this.JobTreeView_BeforeLabelEdit;
            this.jobTreeView.AfterLabelEdit += this.JobTreeView_AfterLabelEdit;
            this.jobTreeView.AfterSelect += this.JobTreeView_AfterSelect;
            this.Clear();
        }

        public void Clear()
        {
            this.jobTreeView.Nodes.Clear();
            this.nodeList.Clear();
            this.InitializeTreeView();
            this.componentListBox.Items.Clear();
        }

        public void AddJobNode(DsComponentGuid guid, String name)
        {
            var node = new TreeNode(name);
            this.projectNode.Nodes.Add(node);

            this.Expand(node);

            this.nodeList.Add(guid, node);
        }

        public void AddComponentNode(DsComponentGuid parentGuid, DsComponentGuid targetGuid, String name)
        {
            if (!this.nodeList.ContainsKey(parentGuid))
            {
                return;
            }

            if (this.nodeList.ContainsKey(targetGuid))
            {
                return;
            }

            TreeNode targetNode = new TreeNode(name);
            TreeNode parentNode = this.nodeList[parentGuid];
            parentNode.Nodes.Add(targetNode);

            // 追加されたノードの親を全て展開
            this.Expand(parentNode);

            this.nodeList.Add(targetGuid, targetNode);
        }

        public void RemoveJobNode(DsComponentGuid guid)
        {
            var targetNode = this.nodeList[guid];
            this.jobTreeView.Nodes.Remove(targetNode);
            this.nodeList.Remove(guid);
        }

        public void RemoveComponentNode(DsComponentGuid guid)
        {
            var targetNode = this.nodeList[guid];
            var parentNode = targetNode.Parent;
            parentNode.Nodes.Remove(targetNode);
            this.nodeList.Remove(guid);
        }

        public void UpdateNodeName(String oldPath, String newPath)
        {
            var nodes = this.projectNode.Nodes;

            var oldNameList = oldPath.Split(DsComponentConstants.Separator);
            var newNameList = newPath.Split(DsComponentConstants.Separator);
            var newName = newNameList[newNameList.Length - 1];
            var index = 0;
            var name = oldNameList[index];

            while (true)
            {
                var foundNode = TreeNodeCollectionUtils.FindTreeNodeByText(nodes, name);
                if (foundNode == null)
                {
                    return;
                }

                index++;

                // 対象のインデックスに達したときは名前を更新して処理を終了
                if (index == oldNameList.Length)
                {
                    foundNode.Text = newName;
                    break;
                }

                name = oldNameList[index];
                nodes = foundNode.Nodes;
            }
        }

        public String GetNewPath(String oldPath, String newName)
        {
            var pos = oldPath.LastIndexOf(DsComponentConstants.Separator);
            if (pos == -1)
            {
                return newName;
            }
            else
            {
                return oldPath.Substring(0, pos) + DsComponentConstants.Separator + newName;
            }
        }

        public void SetComponentListBox(IEnumerable<String> valueList)
        {
            this.componentListBox.Items.Clear();
            this.componentListBox.Items.AddRange(valueList.ToArray());
        }

        public void SelectNode(DsComponentGuid guid)
        {
            if (guid == null)
            {
                this.jobTreeView.SelectedNode = this.projectNode;
                return;
            }

            TreeNode node;
            var isSuccess = this.nodeList.TryGetValue(guid, out node);
            if (!isSuccess)
            {
                return;
            }

            this.jobTreeView.SelectedNode = node;
        }

        internal String RemoveRootName(String path)
        {
            var pos = path.IndexOf("/");
            if (pos == -1)
            {
                Debug.Assert(false);
                return null;
            }

            return path.Substring(pos + 1);
        }

        internal Boolean CanEditNode(String path)
        {
            var pos = path.IndexOf("/");
            return pos != -1;
        }

        private void InitializeTreeView()
        {
            this.projectNode = new TreeNode(Job);
            this.jobTreeView.Nodes.Add(this.projectNode);
        }

        private void Expand(TreeNode targetNode)
        {
            var node = targetNode;
            while (node != null)
            {
                node.Expand();
                node = node.Parent;
            }
        }

        private void JobTreeView_MouseDown(Object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Left && e.Clicks == 2)
            {
                this.doubleClicked = true;
            }
            else
            {
                this.doubleClicked = false;
            }

            if (e.Button == MouseButtons.Right)
            {
                this.jobTreeView.SelectedNode = this.jobTreeView.GetNodeAt(e.X, e.Y);
            }
        }

        private void JobTreeView_BeforeExpand(Object sender, TreeViewCancelEventArgs e)
        {
            if (this.doubleClicked)
            {
                e.Cancel = true;
                this.doubleClicked = false;
            }
        }

        private void JobTreeView_BeforeCollapse(Object sender, TreeViewCancelEventArgs e)
        {
            if (this.doubleClicked)
            {
                e.Cancel = true;
                this.doubleClicked = false;
            }
        }
        
        private void JobTreeView_BeforeLabelEdit(Object sender, NodeLabelEditEventArgs e)
        {
            var oldPath = e.Node.FullPath;
            if (!this.CanEditNode(oldPath))
            {
                e.CancelEdit = true;
            }
        }

        private void JobTreeView_AfterLabelEdit(Object sender, NodeLabelEditEventArgs e)
        {
            var oldPath = e.Node.FullPath;
            var newName = e.Label == null ? "" : e.Label;
            var newPath = this.GetNewPath(oldPath, newName);

            oldPath = this.RemoveRootName(oldPath);
            newPath = this.RemoveRootName(newPath);

            Boolean cancel = false;
            if (e.Node.Parent == this.projectNode && this.afterJobNameEditEventHandler != null)
            {
                this.afterJobNameEditEventHandler(out cancel, oldPath, newPath, newName);
            }
            else if (this.afterComponentNameEditEventHandler != null)
            {
                this.afterComponentNameEditEventHandler(out cancel, oldPath, newPath, newName);
            }

            e.CancelEdit = cancel;
        }

        private void JobTreeView_AfterSelect(Object sender, TreeViewEventArgs e)
        {
            if (e.Node == this.projectNode)
            {
                this.componentListBox.Items.Clear();
                this.treeViewContextMenuStrip.Items.Clear();
            }
            else
            {
                if (this.afterComponentSelectEventHandler != null)
                {
                    var path = this.GetComponentPath(e.Node.FullPath);
                    if (path == null)
                    {
                        return;
                    }

                    this.afterComponentSelectEventHandler(path);
                }

                this.treeViewContextMenuStrip.Items.Clear();
                this.treeViewContextMenuStrip.Items.Add("最新の情報に更新", null, this.JobTreeViewUpdateMenuItem_Click);
            }
        }

        private String GetComponentPath(String path)
        {
            var target = Job + "/";
            var pos = path.IndexOf(target);
            if (pos != 0)
            {
                return null;
            }

            return path.Substring(target.Length);
        }

        private void JobTreeViewUpdateMenuItem_Click(Object sender, EventArgs e)
        {
            if (this.ComponentListUpdateEventaHandler != null)
            {
                var selectedNode = this.jobTreeView.SelectedNode;
                if (selectedNode == null)
                {
                    return;
                }

                var path = this.GetComponentPath(selectedNode.FullPath);
                if (path == null)
                {
                    return;
                }

                this.ComponentListUpdateEventaHandler(path);
            }
        }
    }
}
