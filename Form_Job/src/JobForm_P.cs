﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;

using Yamabuki.Design.Component.Base;
using Yamabuki.Design.Component.RootSystem;
using Yamabuki.EditorDev;
using Yamabuki.EditorDev.Manager;
using Yamabuki.EditorDev.Operation;
using Yamabuki.Utility.Log;
using Yamabuki.Window.ChildForm;
using Yamabuki.Window.ParentForm;
using Yamabuki.Window.Utility;

namespace Yamabuki.Form.Job
{
    public class JobForm_P
        : ChildForm_P<JobForm_VI>
    {
        /// <summary>ロガー</summary>
        private static readonly Logger Logger = LogManager.GetLogger(
            MethodBase.GetCurrentMethod().DeclaringType);

        private String selectedSubSystemPath;

        private Boolean isInitialized;

        private FileManager fileManager;
        
        public JobForm_P(ParentForm_PI parentForm_P)
            : base(parentForm_P)
        {
        }

        public override event Action<Boolean> Form_VisibleChanged
        {
            add { this.View.Form_VisibleChanged += value; }
            remove { this.View.Form_VisibleChanged -= value; }
        }

        public CanvasManager CanvasManager { private get; set; }

        public ComponentManager ComponentManager { private get; set; }

        public OperationUndoStack OperationUndoStack { private get; set; }

        public static ChildForm_PI Create(ParentForm_PI parentForm_PI, DevContext context)
        {
            var presenter = new JobForm_P(parentForm_PI);
            var view = new JobForm_V();
            presenter.ComponentManager = context.ComponentManager;
            presenter.CanvasManager = context.CanvasManager;
            presenter.fileManager = context.FileManager;
            presenter.OperationUndoStack = context.OperationUndoStack;

            presenter.View = view;
            return presenter;
        }

        internal void ComponentManager_Cleared()
        {
            this.View.Clear();
        }

        internal void ComponentManager_JobAdded(DsRootSystem rootSystem)
        {
            this.AddComponent(rootSystem);
        }

        internal void ComponentManager_ComponentAdded(DsComponentImpl parentCom)
        {
            this.AddComponent(parentCom);
        }

        internal void ComponentManager_ComponentRemoved(DsComponentImpl parentCom)
        {
            this.RemoveComponent(parentCom);
        }

        internal void ComponentManager_ComponentNameUpdated(String oldPath, String newPath)
        {
            this.View.UpdateNodeName(oldPath, newPath);
        }

        internal void CanvasManager_Reloaded()
        {
            var systemComponent = this.CanvasManager.ActiveSystemComponent;
            if (systemComponent == null)
            {
                this.View.SelectNode(null);
                return;
            }

            this.View.SelectNode(systemComponent.ComponentGuid);
        }

        internal void AddComponent(DsComponentImpl com)
        {
            if (com.IsRootSystem)
            {
                this.View.AddJobNode(com.Data.ComponentGuid, com.Data.DefinitionName);
            }
            else if (com.IsSubSystem)
            {
                this.View.AddComponentNode(
                    com.Data.ParentComponent.ComponentGuid,
                    com.Data.ComponentGuid,
                    com.Data.DefinitionName);
            }

            foreach (var childCom in com.Data.ChildComponentDataList)
            {
                this.AddComponent(childCom.Component);
            }
        }

        protected override void OnViewSet()
        {
            if (this.isInitialized)
            {
                Debug.Assert(false);
                return;
            }

            this.isInitialized = true;

            base.OnViewSet();

            this.View.Form_Load = this.Form_Load;

            this.View.JobTreeView_DoubleClick = this.JobTreeView_DoubleClick;
            this.View.JobTreeView_AfterJobNameEdit = this.JobTreeView_JobNameEdit;
            this.View.JobTreeView_AfterComponentNameEdit = this.JobTreeView_ComponentNameEdit;
            this.View.JobTreeView_AfterComponentSelect = this.JobTreeView_ComponentSelect;
            this.View.ComponentListUpdateEventaHandler = this.UpdateComponentList;
            this.View.ComponentListBox_DoubleClick = this.ComponentListBox_DoubleClick;

            this.ComponentManager.Cleared += this.ComponentManager_Cleared;
            this.ComponentManager.ComponentNameUpdated += this.ComponentManager_ComponentNameUpdated;
            this.ComponentManager.JobAdded += this.ComponentManager_JobAdded;
            this.ComponentManager.ComponentAdded += this.ComponentManager_ComponentAdded;
            this.ComponentManager.ComponentRemoved += this.ComponentManager_ComponentRemoved;

            this.CanvasManager.Reloaded += this.CanvasManager_Reloaded;
        }

        private static String CheckNodeName(String name)
        {
            if (!DsComponentImpl.IsValidDefinitionName(name))
            {
                return DsComponentImpl.InvalidDefinitionNameCharacter + " は使用できません。";
            }

            if (name.Length == 0)
            {
                return "名前を空欄にはできません。";
            }

            return null;
        }

        private void RemoveComponent(DsComponentImpl com)
        {
            if (com.IsSubSystem)
            {
                this.View.RemoveComponentNode(com.Data.ComponentGuid);
            }
        }

        private void Form_Load()
        {
            try
            {
                Logger.BeginMethod(MethodBase.GetCurrentMethod());

                this.View.Initialize();

                var rootSystemList = this.ComponentManager.ComponentCollection.FindRootSystemList();
                rootSystemList.ToList().ForEach(x => this.AddComponent(x));
            }
            catch (Exception ex)
            {
                Logger.Error(ex.Message, ex);
            }
            finally
            {
                Logger.EndMethod(MethodBase.GetCurrentMethod());
            }
        }

        private void JobTreeView_DoubleClick()
        {
            try
            {
                Logger.BeginMethod(MethodBase.GetCurrentMethod());
                var guid = this.View.SelectedNodeGuid;
                if (guid == null)
                {
                    this.CanvasManager.ReloadCanvas(null);
                }

                var com = this.ComponentManager.ComponentCollection.FindByGuid(guid);
                if (com == null)
                {
                    return;
                }

                if (com.IsRootSystem || com.IsSubSystem)
                {
                    this.CanvasManager.ReloadCanvas(com);
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex.Message, ex);
            }
            finally
            {
                Logger.EndMethod(MethodBase.GetCurrentMethod());
            }
        }

        private void JobTreeView_JobNameEdit(out Boolean cancel, String oldPath, String newPath, String newName)
        {
            cancel = false;
            
            var result = CheckNodeName(newName);
            if (result != null)
            {
                MessageBoxUtils.ShowOkErrorMessage(result, "ノードの編集");
                cancel = true;
                return;
            }

            this.ComponentManager.UpdateComponentNameAndPushUndoStack(oldPath, newPath);
            this.OperationUndoStack.Flush();
        }

        private void JobTreeView_ComponentNameEdit(out Boolean cancel, String oldPath, String newPath, String newName)
        {
            cancel = false;

            var result = CheckNodeName(newName);
            if (result != null)
            {
                MessageBoxUtils.ShowOkErrorMessage(result, "ノードの編集");
                cancel = true;
                return;
            }

            this.ComponentManager.UpdateComponentNameAndPushUndoStack(oldPath, newPath);
            this.OperationUndoStack.Flush();
        }

        private void JobTreeView_ComponentSelect(String path)
        {
            this.UpdateComponentList(path);
        }

        private void UpdateComponentList(String path)
        {
            this.selectedSubSystemPath = path;
            var com = this.ComponentManager.ComponentCollection.FindByDefinitionPath(path);
            if (com == null)
            {
                return;
            }

            var children = this.ComponentManager.ComponentCollection.GetChildrenComponent(com.ComponentGuid);
            this.View.SetComponentListBox(children.Select(x => x.DefinitionName));
        }

        private void ComponentListBox_DoubleClick(String definitionName)
        {
            var definitionPath = this.selectedSubSystemPath + "/" + definitionName;
            var com = this.ComponentManager.ComponentCollection.FindByDefinitionPath(definitionPath);
            if (com != null)
            {
                this.CanvasManager.SetScrollPosition(com.X, com.Y);
            }
        }
    }
}
